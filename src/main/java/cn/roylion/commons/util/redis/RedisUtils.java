package cn.roylion.commons.util.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author: Roylion
 * @Description: Redis工具类
 * @Date: Created in 14:02 2018/10/9
 */
public class RedisUtils {

    private static final String HOST = "172.16.129.253";
    private static final int PORT = 6379;
    private static final String PASSWORD = "Hangzhou@123";

    private static final String OK = "OK";

    private static JedisPool jedisPool;

    static {
        setJedisPool();
    }

    //=============基本命令=============

    // keys *
    public static Set<String> keys(String pattern) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.keys(pattern);
        }
    }

    // exists key [key ...]
    public static long exists(String... keys) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.exists(keys);
        }
    }

    // del key [key ...]
    public static long del(String... keys) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.del(keys);
        }
    }

    // type key
    public static String type(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.type(key);
        }
    }

    // flushall
    public static boolean flushAll() {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.flushAll());
        }
    }


    //=============字符串类型=============

    // get key
    public static String get(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get(key);
        }
    }

    // set key value
    public static boolean set(String key, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.set(key, value));
        }
    }

    // set key value
    public static boolean set(String key, String value, long time) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.psetex(key, time, value));
        }
    }

    // incr key
    public static long incr(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.incr(key);
        }
    }

    // incrby key integer
    public static boolean incrBy(String key, int integer) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.incrBy(key, integer));
        }
    }

    // decr key
    public static boolean decr(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.decr(key));
        }
    }

    // decrby key integer
    public static boolean decrBy(String key, int integer) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.decrBy(key, integer));
        }
    }

    // append key value
    public static long append(String key, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.append(key, value);
        }
    }

    // strlen key
    public static long strlen(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.strlen(key);
        }
    }

    // mget key [key ...]
    public static List<String> mGet(String... keys) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.mget(keys);
        }
    }

    // mset key value [key value ...]
    public static boolean mSet(String... keysValues) {
        if (keysValues.length % 2 != 0) {
            return false;
        }
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.mset(keysValues));
        }
    }

    // getbit key offset
    public static boolean getBit(String key, long offset) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.getbit(key, offset);
        }
    }

    // setbit key offset value
    public static boolean setBit(String key, long offset, boolean value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.setbit(key, offset, value);
        }
    }

    // bitcount key
    public static long bitCount(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.bitcount(key);
        }
    }


    //=============哈希类型=============

    // hgetall key
    public static Map<String, String> hGetAll(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hgetAll(key);
        }
    }

    // hget key field
    public static String hGet(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hget(key, field);
        }
    }

    // hmget key field [field ...]
    public static List<String> hMGet(String key, String... fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hmget(key, fields);
        }
    }

    // hset key field value
    public static boolean hSet(String key, String field, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.hset(key, field, value));
        }
    }

    // hsetnx key field value
    public static boolean hSetNX(String key, String field, String value) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.hsetnx(key, field, value));
        }
    }

    // hmset key field value [field value ...]
    public static boolean hMSet(String key, Map<String, String> fieldsValues) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.hmset(key, fieldsValues));
        }
    }

    // hexists key field
    public static boolean hExists(String key, String field) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.hexists(key, field));
        }
    }

    // hincrby key field integer
    public static boolean hIncrBy(String key, String field, long integer) {
        try (Jedis jedis = jedisPool.getResource()) {
            return OK.equals(jedis.hincrBy(key, field, integer));
        }
    }

    // hdel key field [field ...]
    public static long hDel(String key, String... fields) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hdel(key, fields);
        }
    }

    // hkeys key
    public static Set<String> hKeys(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hkeys(key);
        }
    }

    // hvals key
    public static List<String> hVals(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hvals(key);
        }
    }

    // hlen key
    public static long hLen(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.hlen(key);
        }
    }


    //=============列表类型=============

    // lpush key value [value ...]
    public static long lPush(String key, String... values) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.lpush(key, values);
        }
    }

    // rpush key valye [value ...]
    public static long rPush(String key, String... values) {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.rpush(key, values);
        }
    }



    // 配置jedis连接池
    private static void setJedisPool() {
        JedisPoolConfig config = new JedisPoolConfig();
        // 最大连接数
        config.setMaxTotal(100);
        // 最大空闲连接数
        config.setMaxIdle(10);
        // 检测连接的可用性 @耗性能@
        config.setTestOnBorrow(true);
        jedisPool = new JedisPool(config, HOST, PORT, 2000, PASSWORD);
    }

    public static void main(String[] args) {
        set("name","test");
        System.out.println(get("name"));
    }
}
