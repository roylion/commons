package cn.roylion.commons.util.http;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @Author: Roylion
 * @Description: httpClient请求
 * @Date: Created in 11:20 2018/9/11
 */
public class HttpClientUtils {

    private HttpClientUtils() {
    }

    private static final String CHARSET = "UTF-8";

    private static HttpClientBuilder httpClientBuilder;

    private static CloseableHttpClient getHttpClient() {
        return httpClientBuilder.build();
    }

    static {
        setHttpClientBuilder();
    }

    public static String doGet(String url) throws IOException {
        // 创建http GET请求
        HttpGet httpGet = new HttpGet(url);
        // 执行请求
        CloseableHttpResponse response = getHttpClient().execute(httpGet);
        return resHandler(response);
    }

    public static String doGet(String url, Map<String, String> params) throws URISyntaxException, IOException {
        URIBuilder uriBuilder = new URIBuilder(url);
        // 设置参数
        if (MapUtils.isNotEmpty(params)) {
            uriBuilder.addParameters(map2NVPair(params));
        }
        return doGet(uriBuilder.build().toString());
    }

    public static String doPost(String url, Map<String, String> params, Map<String, String> headers) throws IOException {
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);
        // 设置头部
        if (MapUtils.isNotEmpty(headers)) {
            httpPost.setHeaders(map2Headers(headers));
        }

        // 设置参数
        if (MapUtils.isNotEmpty(params)) {
            // 构造一个form表单式的实体 并设置到httpPost对象中
            httpPost.setEntity(new UrlEncodedFormEntity(map2NVPair(params)));
        }

        // 执行请求
        CloseableHttpResponse response = getHttpClient().execute(httpPost);

        return resHandler(response);
    }

    public static String doPost(String url, Map<String, String> params) throws IOException {
        return doPost(url, params, null);
    }

    public static String doPost(String url) throws IOException {
        return doPost(url, null, null);
    }

    public static String doPostJson(String url, String json, Map<String, String> headers) throws IOException {
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);

        // 设置头部
        if (MapUtils.isNotEmpty(headers)) {
            httpPost.setHeaders(map2Headers(headers));
        }

        // 设置参数
        if (StringUtils.isNotEmpty(json)) {
            // 标识出传递的参数是 application/json
            httpPost.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
        }

        // 执行请求
        CloseableHttpResponse response = getHttpClient().execute(httpPost);
        return resHandler(response);
    }

    public static String doPostJson(String url, String json) throws IOException {
        return doPostJson(url, json, null);
    }

    public static String doPostJson(String url, Map<String, String> params) throws IOException {
        return doPostJson(url, JSONObject.toJSONString(params), null);
    }

    public static String doPostJson(String url, Map<String, String> params, Map<String, String> headers) throws IOException {
        return doPostJson(url, JSONObject.toJSONString(params), headers);
    }

    public static String doFileUpload(String url, byte[] fileBytes, String type) throws IOException {
        HttpPost httpPost = new HttpPost(url);
        MultipartEntityBuilder multi = MultipartEntityBuilder.create();

        multi.addBinaryBody("file", fileBytes, ContentType.create("multipart/form-data"), "cover.jpg");
        multi.addTextBody("file_type", type, ContentType.create("multipart/form-data"));
        httpPost.setEntity(multi.build());

        CloseableHttpResponse response = getHttpClient().execute(httpPost);

        return resHandler(response);
    }

    private static Header[] map2Headers(Map<String, String> headers) {
        Header[] arr = null;
        if (headers != null) {
            arr = new Header[headers.size()];
            // 设置请求头参数
            int i = 0;
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                arr[i++] = new BasicHeader(entry.getKey(), entry.getValue());
            }
        }
        return arr;
    }

    private static List<NameValuePair> map2NVPair(Map<String, String> params) {
        List<NameValuePair> nvPair = new ArrayList<NameValuePair>();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            nvPair.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
        }
        return nvPair;
    }

    // 设置响应内容处理
    private static String resHandler(CloseableHttpResponse response) throws IOException {
        try {
            int statusCode = response.getStatusLine().getStatusCode();//状态码
            HttpEntity httpEntity = response.getEntity();//响应内容
            String result = EntityUtils.toString(httpEntity, CHARSET);
            return result;
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    // 设置连接参数 默认值
    private static void setHttpClientBuilder() {
        PoolingHttpClientConnectionManager pool = new PoolingHttpClientConnectionManager();
        pool.setMaxTotal(200);//设置连接总数
        pool.setDefaultMaxPerRoute(100);//设置每个地址的并发数
        pool.setValidateAfterInactivity(1000);
//        pool.closeExpiredConnections();//关闭失效的连接
        httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setConnectionManager(pool);

        RequestConfig.Builder builder = RequestConfig.custom();
        builder.setConnectionRequestTimeout(500);//从连接池中获取到连接的最长时间
        builder.setConnectTimeout(5000);//设置链接超时
        builder.setSocketTimeout(30000);//数据传输的最长时间
        httpClientBuilder.setDefaultRequestConfig(builder.build());
    }

    public static void main(String[] args) throws IOException {
        System.out.println(doPostJson("http://192.168.52.204:8080/restSMS/rest/authorize", "{\"username\":\"cfetsapi\",\"password\":\"aA123!api\"}", null));
    }
}